Pod::Spec.new do |s|
  s.name = "CoCoReactiveCocoa"
  s.version = "2.5.0.2"
  s.summary = "A framework for composing and transforming streams of values."
  s.license = "MIT"
  s.authors = {"Josh Abernathy"=>"josh@github.com"}
  s.homepage = "https://github.com/blog/1107-reactivecocoa-is-now-open-source"
  s.description = "CoCoReactiveCocoa (RAC) is an Objective-C framework for Functional Reactive Programming. It provides APIs for composing and transforming streams of values."
  s.requires_arc = true
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoReactiveCocoa-framework.git' }
  s.framework = 'MapKit'
  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'CoCoReactiveCocoa.framework'
end
